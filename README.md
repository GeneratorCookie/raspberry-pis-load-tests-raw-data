# Raspberry Pis Load Tests Raw Data (temperature)

Results of temperature load tests I conducted on my Pis to compare different cooling mechanisms, and see how the overclocked CPU would affect them.

Load tests done with [Stressberry](https://github.com/nschloe/stressberry)